#!/bin/bash -x
# setup_env.sh
# Create a default package and create a pinned blueprint environment


main()
{
    # Create a temporary default project
    local tmpdir="_tmp"
    if [[ -d "${tmpdir}" ]]; then
        \rm -rfv "${tmpdir}" || return
    fi
    copier -f --vcs-ref=HEAD . "${tmpdir}" || return

    # Use script in project to create blueprint environment
    local env_name="python-blueprint"
    bash -x "${tmpdir}"/tools/setup_env.sh -n "${env_name}" || return

    # Clean up
    \rm -rv "${tmpdir}" || return

    echo -e "\nconda activate ${env_name}\n"
}


main "${@}"
