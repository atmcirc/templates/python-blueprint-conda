# Python Project Template

This is a [copier](https://copier.readthedocs.io/) template for Python projects, based on the [MCH/APN Python Blueprint](https://github.com/MeteoSwiss-APN/mch-python-blueprint).

## Branches

- `main`: The `main` branch forked from upstream, extended only by this README file.
- `main-conda`: The main branch for conda-based projects; based `main` (which is already based on conda) with some customizations.
- `main-f2py`: The main branch for conda-based projects using [`f2py`](https://numpy.org/doc/stable/f2py/) to incorporate Fortran code; based on `main-conda`.

## Usage

First, if it is not already available, you need to install copier with conda/mamba or pip:

```bash
mamba install -c conda-forge copier
# or
python -m pip install copier
```

### Create a new project

First, create an empty git repository, either manually (`mkdir` and `git init`) or by creating it on github/gitlab (without a README) and cloning it:

```bash
git clone git@git.iac.ethz.ch:atmcirc/tools/my-tool.git
cd my-tool
```

Next, initialize your project with copier from the appropriate template, i.e., `main-*` branch.

For a regular Python project using conda, run:

```bash
copier git@git.iac.ethz.ch:atmcirc/python-project-template --vcs-ref main-conda
```

For a conda-based Python project using [`f2py`](https://numpy.org/doc/stable/f2py/) to incorporate Fortran code, run:

```bash
copier git@git.iac.ethz.ch:atmcirc/python-project-template --vcs-ref main-f2py
```

You will be asked a bunch of questions about you and your project. Copier will use your answers and include them in the generated files, e.g., add your contact information to the project's metadata in `pyproject.toml`. (The answers are stored in `.copier-answers.yaml`, which you should never edit by hand.)

As a last step, add the files to git:

```bash
git add .
git commit -m "create empty project"
push
```

Happy coding!

PS: Don't forget to initialize pre-commit, which has to be done once after creating or cloning a repo:

```bash
pre-commit init
```

Otherwise, all the useful formatters and checkers (aka. linters) that ensure correctness and prevent deterioration of your code won't be run when you commit something.

### Update an existing project

Occasionally, the project templates will be updated, e.g., to keep the linter configurations or documentation up-to-date. In order to propagate such changes into your project, run:

```bash
copier -f update
```

Copier will then merge the changes in the template (the link to which is stored in `.copier-answers.yaml` along with the your answers to the setup questions) into your project, taking the git history of your project into account (so if, for instance, you renamed or removed a file, copier should pick this up). With `-f` or `--force`, copier won't ask you whether to overwrite files in case of a conflict, which is convenient because many of these conflicts that occur during the merge conflict can actually be resolved.

Note that it is also possible to change any answer to the questions copier asked you during project creation (which might be useful even if the template did not change); see `copier update --help` for details.

Copier won't add or commit any changes to git, so you can conveniently review and stage the changes using standard tools like `git add -p`. In case of conflicts, copier will create a patch file ending in `.rej` ("rejected") that contains the respective diff. Once you have reviewed, staged and (if necessary) adapted the changes, commit them and clean up:

```bash
git add -p
# ...
git commit -m "update copier template"
git restore .
git clean -f .
```
